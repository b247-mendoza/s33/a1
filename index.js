//1. Getting all To Do list items
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then(data => console.log(data));


//2. Creating an Array retrieving the title of every item
// fetch('https://jsonplaceholder.typicode.com/todos')
// .then((response) => {(response.forEach(){

// })}
// .then((data) => console.log(data));



//3. Getting a single To Do list item from JSON
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((res) => res.json())
.then((data) => console.log(data));


// 4. Printing the title and staus of the To Do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then(data => console.log(`${data.title}, ${data.completed}`));

// 5. Create a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'new to do list',
		userId: 101
	})
})
.then((response) => response.json()).then((data) => console.log(data));


// 6. Updating a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'updated to do list',
		userId: 101
	})
})
.then((response) => response.json()).then((data) => console.log(data));

// 7. Updating the data structure of to do list properties
fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'another updated to do list',
		description: 'to do list no. 2',
		status: 'completed',
		dateCompleted: new Date,
		userId: 102
	})
})
.then((response) => response.json()).then((data) => console.log(data));

// 8. Using PATCH method to update to do list item
fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'another one updated to do list',
		userId: 101
	})
})
.then((response) => response.json()).then((data) => console.log(data));

// 9. Updating To Do list item's status and date 
fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		status: 'complete',
		date: Date()
	})
})
.then((response) => response.json()).then((data) => console.log(data));

// 10. Deleting an item
fetch('https://jsonplaceholder.typicode.com/todos/101', {
	method: 'DELETE',
})
.then((response) => response.json()).then((data) => console.log(data));

